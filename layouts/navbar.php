<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <nav>
        <ul>
        <?php
            if (session_status() === PHP_SESSION_NONE) {
                session_start();
            }
            if (isset($_SESSION['username'])) {
                // Nếu đã đăng nhập
                echo '<li><a href="logout.php">Đăng xuất</a></li>';
            } else {
                // Nếu chưa đăng nhập
                echo '<li><a href="dangnhap.php">Đăng nhập</a></li>';
                echo '<li><a href="dangky.php">Đăng ký</a></li>';
            }
        ?>
        </ul>
    </nav>