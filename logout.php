<?php
session_start();

// Hủy bỏ phiên đăng nhập
session_destroy();

// Chuyển hướng về trang chủ
header('Location: list.php');
exit();
?>
