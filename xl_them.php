<?php 
$kn = mysqli_connect('localhost', 'root', '', 'qlsanpham');

if (!$kn) {
    die('Kết nối thất bại: ' . mysqli_connect_error());
}

if (isset($_POST['ten'], $_FILES['anh']['name'], $_POST['gia'], $_POST['mota'])) {
    $ten = mysqli_real_escape_string($kn, $_POST['ten']);
    $gia = mysqli_real_escape_string($kn, $_POST['gia']);
    $mota = mysqli_real_escape_string($kn, $_POST['mota']);

    $anh = $_FILES['anh']['name'];
    $anh_path = 'img/' . $anh;
    
    if (!move_uploaded_file($_FILES['anh']['tmp_name'], $anh_path)) {
        die('Lỗi khi tải lên hình ảnh');
    }

    $sql = "INSERT INTO product (ten, anh, gia, mota) VALUES ('$ten', '$anh', '$gia', '$mota')";
    
    $kq = mysqli_query($kn, $sql);

    if (!$kq) {
        die('Lỗi truy vấn: ' . mysqli_error($kn));
    }

    mysqli_close($kn);
    header('Location: list.php');
    exit();
} else {
    echo 'Dữ liệu không đầy đủ';
    exit();
}
?>
