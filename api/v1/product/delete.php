<?php
// File: api/v1/product/delete.php

header('Content-Type: application/json');

// Kiểm tra phương thức
if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    
    //lấy dữ liệu từ file json
    $json_data = file_get_contents("php://input");

    // Đưa về một mảng
    $data = json_decode($json_data, true);
  
    // Validation
    if (!isset($data['id'])) {
        echo json_encode(['error' => 'Không tồn tại id']);
        exit;
    }
    // Lấy ID sp
    $id = $data['id'];

    // Kết nối 
    $kn = mysqli_connect('localhost', 'root', '', 'qlsanpham');
    if (!$kn) {
        die(json_encode(['error' => 'Kết nối cơ sở dữ liệu thất bại']));
    }

    $sql = "DELETE FROM product WHERE id='$id'";
    $result = mysqli_query($kn, $sql);

    if ($result) {
        echo json_encode(['success' => 'Xóa sản phẩm thành công']);
    } else {
        echo json_encode(['error' => 'Lỗi xóa cơ sở dữ liệu']);
    }
    mysqli_close($kn);
} else {
    echo json_encode(['error' => 'Phương thức không hỗ trợ']);
}
?>
