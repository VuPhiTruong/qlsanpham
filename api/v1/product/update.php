<?php
// File: api/v1/product/update.php

header('Content-Type: application/json');

// Kiểm tra phương thức
if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    // Lấy dữ liệu từ request
    $json_data = file_get_contents("php://input");

    //đưa về 1 mảng
    $data = json_decode($json_data, true);
    //validation
    if (!isset($data['id'])) {
      echo json_encode(['error' => 'Không tồn tại id']);
      exit;
    }

    // Lấy dữ liệu
    $id = $data['id'];
    $ten = $data['ten'];
    $anh = $data['anh'];
    $gia = $data['gia'];
    $mota = $data['mota'];
    

    // Kết nối
    $kn = mysqli_connect('localhost', 'root', '', 'qlsanpham');
    if (!$kn) {
        die(json_encode(['error' => 'Kết nối cơ sở dữ liệu thất bại']));
    }

    $sql = "UPDATE product SET ten='$ten',anh='$anh', gia='$gia', mota='$mota'  WHERE id='$id'";
    $result = mysqli_query($kn, $sql);

    // Kiểm tra và trả về kết quả JSON
    if ($result) {
      echo json_encode(['success' => 'Cập nhật sản phẩm thành công']);
    } else {
        echo json_encode(['error' => 'Lỗi cập nhật cơ sở dữ liệu']);
    }

    // Đóng kết nối cơ sở dữ liệu
    mysqli_close($kn);
} else {
    echo json_encode(['error' => 'Phương thức không hỗ trợ']);
}
?>
