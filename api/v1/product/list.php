<?php 
//file: api/v1/product/list.php

header('Content-type: application/json');

//kết nối
$kn = mysqli_connect('localhost', 'root', '', 'qlsanpham');
if (!$kn) {
    die(json_encode(['error' => 'Kết nối thất bại']));
}

//truy vấn
$sql = "SELECT * FROM product";
$result = mysqli_query($kn, $sql);

//request

if($result){
  $products = mysqli_fetch_all($result, MYSQLI_ASSOC);
  echo json_encode(['products' => $products]);
} else {
  echo json_encode(['error' => 'Lỗi truy vấn']);
}
mysqli_close($kn);
?>