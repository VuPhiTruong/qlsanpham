<?php
$kn = mysqli_connect('localhost', 'root', '', 'qlsanpham');
if (!$kn) {
    echo "Kết nối thất bại";
}

$id = $_GET['id'];

// Lấy tên file ảnh trước khi xóa
$sql_get_image = "SELECT anh FROM product WHERE id='$id'";
$result_get_image = mysqli_query($kn, $sql_get_image);
$row_image = mysqli_fetch_assoc($result_get_image);
$anh_to_delete = $row_image['anh'];

// Xóa dữ liệu từ CSDL
$sql_delete = "DELETE FROM product WHERE id='$id'";
$result_delete = mysqli_query($kn, $sql_delete);

if ($result_delete) {
    // Kiểm tra và xóa file ảnh từ thư mục
    if ($anh_to_delete && file_exists("img/$anh_to_delete")) {
        unlink("img/$anh_to_delete");
    }

    // Chuyển hướng về trang danh sách
    header('location:list.php');
} else {
    echo "Lỗi truy vấn: " . mysqli_error($kn);
}

mysqli_close($kn);
?>
