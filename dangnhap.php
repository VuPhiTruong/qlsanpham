<?php
include_once('layouts/navbar.php');
session_start();
if (isset($_GET['message'])) {
    $message = $_GET['message'];
    if ($message == 'success') {
        echo 'Đăng ký thành công. Đăng nhập ngay!';
    }
    if($message == 'error_pass') {
        echo 'Sai mật khẩu!';
    }
    if($message == 'error_not_found') {
        echo 'Tài khoản không tồn tại!';
    }
}

?>
    <h1>Đăng nhập</h1>
    <form class="login_register" action="xl_dangnhap.php" method="POST">
        <label for="username">Tên người dùng:</label>
        <input type="text" id="username" name="username" required><br>
        
        <label for="password">Mật khẩu:</label>
        <input type="password" id="password" name="password" required><br>

        <input type="submit" value="Đăng nhập">
    </form>
<?php
include_once('layouts/footer.php');
?>
